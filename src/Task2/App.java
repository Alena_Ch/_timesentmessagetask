package Task2;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Scanner;

public class App {

    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        System.out.println("Введите ближайшее время отправки сообщений в формате:\n YYYY\n MM\n DD\n HH\n MM\n SS\n");
        String year = scn.next();
        int y = Integer.parseInt(year);
        String month = scn.next();
        int m = Integer.parseInt(month);
        String day = scn.next();
        int d = Integer.parseInt(day);
        String hour = scn.next();
        int h = Integer.parseInt(hour);
        String min = scn.next();
        int mi = Integer.parseInt(min);
        String sec = scn.next();
        int s = Integer.parseInt(sec);
        LocalDateTime timeOfSending = LocalDateTime.of(y,m,d,h,mi,s);
        LocalDateTime timeNow = LocalDateTime.now();
        timeNow.getMinute();
        System.out.println("Сейчас: " + timeNow);
        Duration timeUntilSending = Duration.between(timeNow, timeOfSending);
        int p = 60;
        long t = timeUntilSending.getSeconds();
        long minutes = t / p;
        long seconds = t - minutes * p;
        System.out.println("Время до отправки сообщения: " + minutes + " минут, " + seconds + " секунд.");
    }
}
